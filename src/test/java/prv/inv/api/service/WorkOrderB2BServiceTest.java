package prv.inv.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import prv.inv.api.entity.ScheduledFixingB2B;

import java.util.List;

import static org.junit.Assert.*;

@Configuration
@ComponentScan(basePackages = "prv.inv.api")
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class WorkOrderB2BServiceTest {

    @Autowired
    private WorkOrderB2BService workOrderB2BService;

    @Test
    public void getAllWorkOrders(){

        List<ScheduledFixingB2B> list = workOrderB2BService.readWorkOrderBySubscription("sub1",false);

        assertTrue(!list.isEmpty());
    }
}