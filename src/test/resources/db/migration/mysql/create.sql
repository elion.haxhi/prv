CREATE TABLE Z (
                   id             BIGINT NOT NULL ,
                   value        INTEGER,
                   primary key (id)
);

CREATE TABLE scheduled_fixing_esiti (
                                        id             BIGINT NOT NULL ,
                                        esito          varchar,
                                        descrizione_errore varchar ,
                                        subscription_id varchar ,
                                        retry_count integer,
                                        primary key (id)
);

CREATE TABLE scheduled_fixing_fake (
                                       id             BIGINT NOT NULL,
                                       start_date          date ,
                                       end_date date ,
                                       subscription_id varchar ,
                                       work_order_id varchar ,
                                       status varchar ,
                                       lavorato boolean,
                                       primary key (id)
);
