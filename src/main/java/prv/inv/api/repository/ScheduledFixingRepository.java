package prv.inv.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import prv.inv.api.entity.ScheduledFixingB2B;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface ScheduledFixingRepository extends JpaRepository<ScheduledFixingB2B,Long>{

    @Query("select s from ScheduledFixingB2B as s where s.subscriptionId =:subscriptionId and s.lavorato =:lavorato ")
    List<ScheduledFixingB2B> findScheduledFixingB2BBySubscriptiionAndLavorato(@Param("subscriptionId") String subscriptionId,
                                                                                      @Param("lavorato") Boolean lavorato);
}
