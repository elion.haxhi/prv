package prv.inv.api.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "scheduled_fixing_esiti")
public class ScheduledFixingEsitiB2B {

    @Id
    private Long id;
    @Column(name = "esito")
    private String esito;
    @Column(name = "descrizione_errore")
    private String descrizioneErrore;
    @Column(name = "subscription_id")
    private String subscriptionId;
    @Column(name = "retry_count")
    private Integer retryCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public String getDescrizioneErrore() {
        return descrizioneErrore;
    }

    public void setDescrizioneErrore(String descrizioneErrore) {
        this.descrizioneErrore = descrizioneErrore;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduledFixingEsitiB2B that = (ScheduledFixingEsitiB2B) o;
        return id.equals(that.id) &&
                Objects.equals(esito, that.esito) &&
                Objects.equals(descrizioneErrore, that.descrizioneErrore) &&
                Objects.equals(subscriptionId, that.subscriptionId) &&
                Objects.equals(retryCount, that.retryCount);
    }

    @Override
    public int hashCode() {
        Long i;
        int result = 17;
        i = getId();
        result = 37*result + i.intValue();
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[ScheduledFixingEsitiB2B |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }
}
