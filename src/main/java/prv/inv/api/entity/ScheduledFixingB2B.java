package prv.inv.api.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "scheduled_fixing_fake")
public class ScheduledFixingB2B {
    @Id
    private Long id;
    @Column(name = "start_date")
    private LocalDate startDate;
    @Column(name = "end_date")
    private LocalDate endDate;
    @Column(name = "subscription_id")
    private String subscriptionId;
    @Column(name = "work_order_id")
    private String workOrderId;
    @Column(name = "status")
    private String status;
    @Column(name = "lavorato")
    private Boolean lavorato;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public Boolean getLavorato() {
        return lavorato;
    }

    public void setLavorato(Boolean lavorato) {
        this.lavorato = lavorato;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduledFixingB2B that = (ScheduledFixingB2B) o;
        return id.equals(that.id) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(subscriptionId, that.subscriptionId) &&
                Objects.equals(workOrderId, that.workOrderId) &&
                status == that.status &&
                Objects.equals(lavorato, that.lavorato);
    }

    @Override
    public int hashCode() {
        Long i;
        int result = 17;
        i = getId();
        result = 37*result + i.intValue();
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[ScheduledFixingB2B |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }
}
