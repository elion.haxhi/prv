package prv.inv.api.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import prv.inv.api.entity.ScheduledFixingB2B;
import prv.inv.api.repository.ScheduledFixingRepository;

import java.util.List;

@Service
public class WorkOrderB2BService {

    private ScheduledFixingRepository scheduledFixingRepository;

    public WorkOrderB2BService(ScheduledFixingRepository scheduledFixingRepository){
        this.scheduledFixingRepository = scheduledFixingRepository;
    }
    public List<ScheduledFixingB2B> readWorkOrderBySubscription(String subscriptionId, Boolean lavorato) {


        return scheduledFixingRepository.findScheduledFixingB2BBySubscriptiionAndLavorato(subscriptionId,lavorato);
    }
}
